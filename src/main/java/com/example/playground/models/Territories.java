package com.example.playground.models;


import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("territories")
public class Territories {

    @Id
    private String territoryId;
    private String territoryDescription;
    private Long regionId;

}
