package com.example.playground.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Table;

@Table("employee_territories")

@Getter
@Setter
public class EmployeeTerritories {

    private Long employeeId;
    private String territoryId;
}
