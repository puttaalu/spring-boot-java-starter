package com.example.playground.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("region")

@Getter
@Setter
public class Region {

    @Id
    private Long regionId;
    private String regionDescription;

}
