package com.example.playground.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("shippers")

@Getter
@Setter
public class Shipper {

    @Id
    private Long shipperId;
    private String companyName;
    private String phone;


}
