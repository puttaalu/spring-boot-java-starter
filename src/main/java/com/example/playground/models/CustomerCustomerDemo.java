package com.example.playground.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Table;

@Table("customer_customer_demo")

@Getter
@Setter
public class CustomerCustomerDemo {

    private String customerId;
    private String customerTypeId;
}
