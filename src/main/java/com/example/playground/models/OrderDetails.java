package com.example.playground.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Table;

@Table("order_details")

@Getter
@Setter
public class OrderDetails {

    private Long orderId;
    private Long productId;
    private double unitPrice;
    private Long quantity;
    private double discount;
}
