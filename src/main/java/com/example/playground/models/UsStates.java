package com.example.playground.models;


import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("us_states")
public class UsStates {

    @Id
    private Long stateId;
    private String stateName;
    private String stateAbbr;
    private String stateRegion;

}
