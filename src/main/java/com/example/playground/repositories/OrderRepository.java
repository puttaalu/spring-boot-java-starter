package com.example.playground.repositories;

import com.example.playground.models.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * @author aashiks on 29/01/2021
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
}
