package com.example.playground;

import org.testcontainers.containers.PostgreSQLContainer;

/**
 * @author aashiks on 02/02/2021
 */
public class DBContainer extends PostgreSQLContainer<DBContainer> {
    private static final String IMAGE_VERSION = "postgres:13";
    private static DBContainer container;

    private DBContainer() {
        super(IMAGE_VERSION);
    }

    public static DBContainer getInstance() {
        if (container == null) {
            container = new DBContainer()
                    .withDatabaseName("testdb")
                    .withUsername("test")
                    .withPassword("test");
            container.start();
        }
        return container;
    }
}
