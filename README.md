# Points of interest

1. Optional return type and ResponseEntity.of method in controllers.
2. Optional return type in repository interface.
3. Constructor autowiring as opposed to field injection.
4. Flyway migration setup
   - After Migrate sql file for seeding data.
5. Testcontainer setup
